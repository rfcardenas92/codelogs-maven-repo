## Agrega los repositorios

Agrega el repositorio en el pom.xml  de tu aplicación.

```xml
   <repositories>
        <repository>
            <id>codelogs-maven-repo-snapshots</id>
            <name>Repositorio codelogs versiones estables</name>
            <releases>
                <enabled>false</enabled>
            </releases>
            <snapshots>
                <enabled>true</enabled>
            </snapshots>
            <url>https://gitlab.com/rfcardenas92/codelogs-maven-repo/raw/snapshots</url>
        </repository>
    </repositories>
```
# Agrega las dependencias Ejm

Agrega las librerias al proyecto

Existen dos ramas 
 - snapshots  cambiar en  {rama}
 - releases   cambiar en  {rama}
 
cambia el url del servidor https://gitlab.com/rfcardenas92/codelogs-maven-repo/raw/{rama} 
si necesitas trabar con librerias contruidas no estables

```xml
    <dependencies>
         <dependency>
            <groupId>ec.edu.utpl.datalab.rexcode</groupId>
            <artifactId>rexcode-monitor-service</artifactId>
            <version>1.0-SNAPSHOT</version>
            <type>jar</type>
        </dependency>
    </dependencies>
```